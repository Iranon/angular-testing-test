import { TestBed, waitForAsync } from '@angular/core/testing';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpGetterService } from './http-getter.service';
// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('HttpGetterService', () => {
  let service: HttpGetterService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, HttpClientTestingModule],
      providers: [ HttpGetterService ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(HttpGetterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a well formatted response', () => {
    const endpoint: string = "https://dog.ceo/api/breeds/image/random";
    const fakeResponse = {message: "https://dog.photo", status: 'success'};
    httpClient.get<{message: string, status: string}>(endpoint)
      .subscribe(data =>
        // When observable resolves, result should match test data
        expect(data).toEqual(fakeResponse)
      );
    const req = httpTestingController.expectOne(endpoint);
    expect(req.request.method).toEqual('GET');
    req.flush(fakeResponse);
    httpTestingController.verify();
  })
});