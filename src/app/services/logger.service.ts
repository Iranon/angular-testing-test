import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  logStatus: boolean = false;
  logInfo: boolean = false;

  setLogStatus = (value: boolean) => {
    this.logStatus = value;
    this.logInfo = this.logStatus;
  }
  getLogStatus = () => this.logStatus;

}