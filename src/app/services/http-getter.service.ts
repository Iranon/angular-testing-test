import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpGetterService {

  constructor(private http: HttpClient) { }

  request = (url: string) => this.http.get<{message: string, status: string}>(url).toPromise();

}