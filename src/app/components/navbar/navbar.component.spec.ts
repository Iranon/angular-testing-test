import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { LoggerService } from 'src/app/services/logger.service';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let element: HTMLElement;
  let loggerServiceStub: Partial<LoggerService>; //stubbed service

  beforeEach(async () => {
    //setting mocked service
    loggerServiceStub = { logStatus: false, logInfo: false };
    //configuring Testing Module (providing even the mocked service as a LoggerService)
    await TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      //providers: [ LoggerService ] //real service
      providers: [
        { provide: LoggerService, useValue: loggerServiceStub } //mocked service (stubbed)
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    TestBed.inject(LoggerService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.nativeElement; //let access to its childs

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be not logged and show "Login" option', () => {
    loggerServiceStub.logInfo = false; //Not logged
    fixture.detectChanges();
    expect(component.logged).toBe(false);
    expect(component.actionLog).toBe('Login');
  });

  it('should logged and show "Logout" option', () => {
    loggerServiceStub.logInfo = true; //Logged
    fixture.detectChanges();
    expect(component.logged).toBe(true);
    expect(component.actionLog).toBe('Logout');
  });

});
