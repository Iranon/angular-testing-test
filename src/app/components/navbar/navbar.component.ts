import { Component } from '@angular/core';
import { LoggerService } from '../../services/logger.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(
    private logger: LoggerService,
  ) { }

  logged: boolean = false;
  actionLog: string = "Login";

  get logInfo(): boolean {
    return this.logger.logInfo;
  }

  logOut = () => {
    this.actionLog = "Login";
    this.logged = false;
    this.logger.setLogStatus(false)
  }

  ngDoCheck() {
    this.logInfo && (this.logged = true);
    this.logInfo && (this.actionLog = this.logged ? "Logout" : "Login");
  }

}