import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { AbstractControl, FormBuilder, ValidationErrors } from '@angular/forms';
import { LoggerService } from 'src/app/services/logger.service';
import { MatSnackBarModule } from '@angular/material/snack-bar'; 
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let element: HTMLElement;
  let formControls: { [key: string]: AbstractControl };
  let loggerServiceStub: Partial<LoggerService>;
  let router: Router;

  beforeEach(async () => {
    //Stub service
    loggerServiceStub = {
      logStatus: false,
      logInfo: false,
      setLogStatus: () => null,
      getLogStatus: () => true
    }

    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ MatSnackBarModule, RouterTestingModule.withRoutes([]), ],
      //providers: [ LoggerService ] //real service
      providers: [
        FormBuilder,
        { provide: LoggerService, useValue: loggerServiceStub } //stubbed service
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    TestBed.inject(LoggerService);
    router = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.nativeElement; //let access to its childs
    formControls = component.loginForm.controls;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('User Name field validity', () => {
    let errors: ValidationErrors;
    const userNameField = formControls['userName'];
    userNameField.setValue(''); //this way it will be invalid (default value is "admin")
    expect(userNameField.valid).toBeFalsy();

    //Field required
    errors = userNameField.errors || {};
    expect(errors['required']).toBeTruthy();

    //Set field test
    userNameField.setValue("testName");
    errors = userNameField.errors || {};
    expect(errors['required']).toBeFalsy();
  });

  it('Password field validity', () => {
    let errors: ValidationErrors;
    const passwordField = formControls['password'];
    expect(passwordField.valid).toBeFalsy(); //It's not valid beacuse it is still empty

    //Field required
    errors = passwordField.errors || {};
    expect(errors['required']).toBeTruthy();

    //Required satisfied
    passwordField.setValue("testPassword");
    errors = passwordField.errors || {};
    expect(errors['required']).toBeFalsy();

    //Min length NOT satisfied
    passwordField.setValue("01234"); //less than 8 digits
    errors = passwordField.errors || {};
    expect(errors['minlength']).toBeTruthy();

    //Min length satisfied
    passwordField.setValue("012345678"); //more than 8 digits
    errors = passwordField.errors || {};
    expect(errors['minlength']).toBeFalsy();
  });

  it('should login and navigate to the HomeComponent (path: "")', () => {
    const navigateSpy = spyOn(router, 'navigate');
    //Form settings
    formControls['userName'].setValue(component.correctUserName);
    formControls['password'].setValue(component.correctPassword);
    expect(component.loginForm.valid).toBeTruthy(); //validity
    loggerServiceStub.logStatus = true;
    component.check();
    expect(navigateSpy).toHaveBeenCalledWith([""]);
  });

});
