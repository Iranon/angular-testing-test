import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoggerService } from 'src/app/services/logger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private logger: LoggerService,
    private router: Router
  ) { }

  loginForm = this.formBuilder.group({
    userName: ['admin', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });

  correctUserName: string = 'admin';
  correctPassword: string = 'admin000'
  wrongUserName?: boolean;
  wrongPassword?: boolean;
  submitted : boolean = false;

  //-Getters
  get userName() {
    return this.loginForm.get('userName');
  };
  get password() {
    return this.loginForm.get('password');
  };

  //- Methods

  userNameChanged = () => {
    this.submitted = false;
    this.wrongUserName = false;
  }

  passwordChanged = () => {
    this.submitted = false;
    this.wrongPassword = false;
  }

  check = () => {
    this.submitted = true;
    if (this.userName!.value === this.correctUserName && this.password!.value === this.correctPassword) {
      this.logger.setLogStatus(true)
      this.wrongPassword = false;
      this.wrongUserName = false;
      this.submitted = false
    }
    //wrong password
    this.userName!.value === this.correctUserName && this.password!.value !== this.correctPassword &&
      (this.wrongPassword = true) && this.password?.valid && (this.openSnackBar('Password Errata'));
    //wrong username
    this.userName!.value !== this.correctUserName && this.password!.value === this.correctPassword &&
      (this.wrongUserName = true) && this.userName?.valid && (this.openSnackBar('Username Errato'));
    //wrong username and password
    this.userName!.value !== this.correctUserName && this.password!.value !== this.correctPassword &&
      (this.wrongUserName = true) && (this.wrongPassword = true) &&
      this.userName?.valid && this.password?.valid && (this.openSnackBar("Utente e password errati"));
    //Go to home if logged in
    this.logger.getLogStatus() && (this.router.navigate([""]));
  }

  openSnackBar = (message: string) => this.snackBar.open(
      message,
      'Nascondi',
      {
        duration: 3000,
        verticalPosition: 'top',
        panelClass: ['custom-snackbar'] }
    );

}
