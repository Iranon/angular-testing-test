import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
  it('should check the new input', () => {
    const emailExampleInput: HTMLInputElement = compiled.querySelector('exampleInputEmail1')!;
    emailExampleInput!.value = "prova@test.lab";
    emailExampleInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(emailExampleInput.value).toBe('prova@test.lab');
  });
  */

});
