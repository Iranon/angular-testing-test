import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Section1Component } from './section1.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpGetterService } from '../../services/http-getter.service';
import { of } from 'rxjs';

describe('Section1Component', () => {
  let component: Section1Component;
  let fixture: ComponentFixture<Section1Component>;
  let service: HttpGetterService
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Section1Component ],
      imports: [ HttpClientModule ],
      providers: [ HttpGetterService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Section1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new HttpGetterService(httpClientSpy);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should bind the title', () => {
    const h1: HTMLElement = fixture.nativeElement.querySelector('h1');
    expect(h1.textContent).toContain(component.title); //it works because the fixture.detectChanges()
  })

  it('the service should return a consistent response', async () => {
    const endpoint: string = "https://dog.ceo/api/breeds/image/random";
    const fakeResponse = {message: "https//:dog", status: 'success'};
    httpClientSpy.get.and.returnValue(of(fakeResponse));
    const mockedRes = await service.request(endpoint);
    expect(mockedRes).toEqual(fakeResponse);
  });

  /*
  it('should be a .jpg image URL', async () => {
    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img')?.src).toMatch(/(.*)\.jpg/);
  });
  */

});
