import { Component, OnInit } from '@angular/core';
import { HttpGetterService } from 'src/app/services/http-getter.service';

@Component({
  selector: 'app-section1',
  templateUrl: './section1.component.html',
  styleUrls: ['./section1.component.scss']
})
export class Section1Component implements OnInit {
  constructor(private httpGetterService: HttpGetterService) { }
  title: string = 'Section 1';
  endpoint: string = "https://dog.ceo/api/breeds/image/random";
  linkFromAPI: string = "";

  async ngOnInit(): Promise<void> {
    this.linkFromAPI = (await this.httpGetterService.request(this.endpoint)).message;
    console.log(this.linkFromAPI);
  }

}
